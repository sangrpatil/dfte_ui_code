import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { TrendsComponent } from './trends/trends.component';
import { HeaderComponent } from './header/header.component';
import { HelpPageComponent } from './help-page/help-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
	  pathMatch: 'full'
  },
  {
    path: 'home',
    component: HeaderComponent,
    children: [
      {
        path: 'reporting',
        component: TrendsComponent
      },
      {
        path: 'validation',
        component: LoginComponent
      },
      {
        path: 'help',
        component: HelpPageComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
