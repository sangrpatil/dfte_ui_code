import { Component, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import {RestApiService} from '../rest-api.service'
import * as CanvasJS from '../../assets/externalJS/canvasjs';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

export interface table1Data  {
  Error_Description: string, 
  Error_Count: string,
  vs_Prior: string
}

export interface table2Data  {
  Batch_date: string, 
  Validation_Date: string,
  Table_Name: string, 
  Error_Description: string, 
  Error_Count: string,
  Error_Severity: string,
  Source_File: string
}

@Component({
  selector: 'app-trends',
  templateUrl: './trends.component.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./trends.component.scss']
})
export class TrendsComponent implements OnInit {
  
  @ViewChild('TableOnePaginator') paginator1: MatPaginator;
  @ViewChild('TableTwoPaginator') paginator2: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  showChart1 = true;
  showChart2 = true;
  showValidationTable = true;
  showErrorTrendTable = true;
  topChartTitle: any;
  connectionSet: any;
  filterValues = {};
  filterSelectObj = [];
  table1_headers: string[] = ["Error_Description", "Error_Count", "vs_Prior"];
  table2_headers: string[] = ["Batch_date", "Validation_Date", "Table_Name", "Error_Description", "Error_Count", "Error_Severity", "Source_File"]
  table1_data: any[];
  table2_data: any[];
  table1Source: any;
  table2Source: any;
  rollingPeriod = "Current Month";
  selectedMonth;
  chart1Data = [];
  chart2Data = [];
  innerObj;
  tempDataPoints = [];
  months = [];
  columnChartColor = ["#749911", "gray", '#005da6', '#b4eeb4', '#528b8b', '#000', '#13f3d1', '#006400', '#1e90ff'];
  errorDescriptionList = ["Mandatory Field is missing","Numeric data type check","Duplicates in Primary Key/Composite Key","Date format check", "Negative value check", "SCD check", "Special characters", "True duplicates"]
  selectedErrorDescription = "Duplicates in Primary Key/Composite Key"
  periods = [
    {
      "title":"Current Month",
      "value": "Current Month"
     },
     {
       "title": "Last 3 Months",
       "value": "3"
     },
     {
       "title": "Last 6 Months",
       "value": "6"
     }
    ];
    info = [{
      "error-trend" : "", 
      "top10-graph": "The graph displays 10 tables with the maximum number of error instances", 
      "execution-summary": "", 
      "error-graph": ""
    }];

    filteredValues = {
      batch_date: '', table_name: '', error_desc: '', error_count: '', error_severity: '', source_file_flag: ''};

  constructor(private restApi: RestApiService) { 
    // Object to create Filter for
    this.filterSelectObj = [
      {
        name: 'Batch Date',
        columnProp: 'batch_date',
        options: []
      }, {
        name: 'Table Name',
        columnProp: 'table_name',
        options: []
      },  {
        name: 'Error Description',
        columnProp: 'error_desc',
        options: []
      }
    ]
  }

  ngOnInit() {
    this.restApi.validateConnection('').subscribe(res=> {
      if(res["status"]== "Success") {
        this.connectionSet = "success";
        let date = new Date();
        let months = [],
            monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];
        for(let i = 0; i < 12; i++) {
            let m = date.getMonth() + 1;
            months.push({"value": "month="+ m + "&year=" + date.getFullYear(), "title" :monthNames[date.getMonth()] + ' ' + date.getFullYear()});
            
            // Subtract a month each time
            date.setMonth(date.getMonth() - 1);
        }
        this.months = months.splice(0, 6);
        this.selectedMonth = this.months[0].value;
        this.getBottomSectionData();
        this.renderTopErrorChart();
        this.renderChart1();
      } else {
        this.connectionSet = 'failure';
      }
    })
  }

  getValidationTableData(param) {
    this.restApi.getReportingTableData2(param).subscribe(res=> {
      if(res["data"].length == 0) {
        this.showValidationTable = false;
      } else {
        this.showValidationTable = true;
        this.table2_data = res["data"];
        this.table2Source = new MatTableDataSource<table2Data>(this.table2_data);
        setTimeout(() => (this.table2Source.paginator = this.paginator2));
        this.filterSelectObj.filter((o) => {
          o.options = this.getFilterObject(this.table2Source.data, o.columnProp);
        });
        // Overrride default filter behaviour of Material Datatable
      //this.table2Source.filterPredicate = this.createFilter();
      this.table2Source.filterPredicate = this.customFilterPredicate();
      }
      
    })
  }

  getBottomSectionData() {
    this.restApi.getReportingTableData1(this.selectedMonth).subscribe(res=> {
      if(res["data"].length == 0) {
        this.showErrorTrendTable = false;
      } else {
        this.showErrorTrendTable = true;
        this.table1_data = res["data"];
        this.table1Source = new MatTableDataSource<table1Data>(this.table1_data);
        setTimeout(() => (this.table1Source.paginator = this.paginator1));
      }
    })
  }

  monthChanged(value) {
    this.getBottomSectionData();
    this.renderTopErrorChart();
  }

  rollingPeriodChanged(value) {
    this.showChart1 = true;
    if(value == "Current Month") {
      this.renderChart1();
    } else {
      this.renderChart2();
    }
  }

  renderChart1() {
    this.getValidationTableData('');
    this.restApi.getMonthlyErrorCount().subscribe(res=> {
      if(res["data"].length == 0) {
        this.showChart1 = false;
      } else {
        this.showChart1 = true;
        this.chart1Data = [];
        let count = 0;
        let that = this;
        for(let key in res["data"]) {
          let obj = {};
          for(let k in res["data"][key]) {
            let value = res["data"][key][k];
            this.innerObj = {};
            for(let i in value) {
              if(i == "date") {
                this.innerObj["label"] = value[i];
              } else {
                this.innerObj["y"] = Number(value[i]);
              }
            }
            that.tempDataPoints.push(this.innerObj);
          }
          obj = {
            type: "column",
            showInLegend: true,
            name: key,
            color: that.columnChartColor[count],
            dataPoints: that.tempDataPoints
          }
          that.chart1Data.push(obj);
          count++;
          that.tempDataPoints = [];
        }
        var chart = new CanvasJS.Chart("chart1", {
          width: 700,
          height: 300,
          animationEnabled: true,
          title:{
            text: ""
          },
          axisY: {
            title: ""
          },
          toolTip:{   
            content: "<strong>Error:</strong> {name}<br> <strong>Count: </strong> {y}"      
          },
          legend: {
            cursor:"pointer",
            itemclick : toggleDataSeries
          },
          data: this.chart1Data
        });
        this.topChartTitle = "Error Count By Error Type";
        chart.render();
      }  
      function toggleDataSeries(e) {
        if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
          e.dataSeries.visible = false;
        }
        else {
          e.dataSeries.visible = true;
        }
        chart.render();
      }
    })
    
  }

  renderChart2() {
    this.getValidationTableData(this.rollingPeriod);
    this.restApi.getCumulativeErrorCount(this.rollingPeriod).subscribe(res=> {
      if(res["data"].length == 0) {
        this.showChart1 = false;
      } else {
        this.showChart1 = true;
        this.chart2Data = [];
        let that = this;
        for(let key in res["data"]) {
          that.chart2Data.push({y : res["data"][key]["count"], name: res["data"][key]["name"]})
        }
        var chart = new CanvasJS.Chart("chart1", {
          width: 700,
          height: 300,
          animationEnabled: true,
          title:{
            text: ""
          },
          legend:{
            cursor: "pointer"
          },
          data: [{
            type: "pie",
            showInLegend: true,
            toolTipContent: "{name}: <strong>{y}</strong>",
            indexLabel: "{name} - {y}",
            dataPoints: this.chart2Data
          }]
        });

        this.topChartTitle = "Cumulative Error Count for Past " + this.rollingPeriod + " Months";
        chart.render();
      }
      
    })
  }

  renderTopErrorChart() {    
    this.showChart2 = true;
    let param = {
      "error_desc": this.selectedErrorDescription,
      "date": this.selectedMonth
    }
    this.restApi.getTopTableErrorCount(param).subscribe(res=> {
      let data = res["data"];
      if(data.length == 0) {
        this.showChart2 = false;
      } else {
        this.showChart2 = true;
        let datapoints = [];
        for(let key in data) {
          datapoints.push({"y": data[key]["err_cnt"], "label": data[key]["tablename"]})
        }
        var chart = new CanvasJS.Chart("chart2", {
          width: 800,
          height: 300,
          animationEnabled: true,
          theme: "light2",
          title:{
            text: ""
          },
          axisY:{
            includeZero: false
          },
          axisX:{
            labelAngle: 125
          },
          toolTip:{   
            content: "<strong>Table Name:</strong> {label}<br> <strong>Error Instances: </strong> {y}"      
          },
          data: [{        
            type: "line",
            lineColor: "#70c730`",
            markerColor: "#006400",
            indexLabelFontSize: 16,
            dataPoints: datapoints
          }]
        });
        chart.render();
      }
    })
  }

  // Get Uniqu values from columns to build filter
  getFilterObject(fullObj, key) {
    const uniqChk = [];
    fullObj.filter((obj) => {
      if (!uniqChk.includes(obj[key])) {
        uniqChk.push(obj[key]);
      }
      return obj;
    });
    return uniqChk;
  }

  // Called on Filter change
  filterChange(filter, event) {
    //let filterValues = {}
    this.filterValues[filter.columnProp] = event.target.value.trim();
    this.table2Source.filter = JSON.stringify(this.filterValues)
  }

  // Custom filter method fot Angular Material Datatable
  customFilterPredicate() {
    const myFilterPredicate = (data: table2Data, filter: string): boolean => {
      let searchString = JSON.parse(filter);
      let colVal = [];
      for(let col in searchString) {
        if(searchString[col] != "") {
          colVal.push(col);
        }
      }
      let value;
      for (let k in colVal) {
        if(Number(k) == 0) {
          value = data[colVal[k]].toString().trim().indexOf(searchString[colVal[k]]) != -1;
        } else {
          value = data[colVal[k]].toString().trim().indexOf(searchString[colVal[k]]) != -1 && value;
        }
      }
      return value;
    }
    return myFilterPredicate;
  }

  // Reset table filters
  resetFilters() {
    this.filterValues = {}
    this.filterSelectObj.forEach((value, key) => {
      value.modelValue = undefined;
    })
    this.table2Source.filter = "";
  }
}
