import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  public dateStart: number;
  public dateEnd: number;
  public keyword: string;

  constructor(private http: HttpClient) { }

  getExecutionSummary(param) {
    return this.http.get(environment.base_url + 'validate/summary-report?schema=' + param.schema + '&table=' + param.table + '&validation_rule=' + param.validation_rule);
  }

  getMonthlyErrorCount() {
    return this.http.get(environment.base_url + 'error/count-by-type');
  }

  getCumulativeErrorCount(param) {
    return this.http.get(environment.base_url + 'error/cumulative-count?rolling_period=' + param)
  }

  getTopTableErrorCount(param) {
    //return this.http.get('/assets/data/graph2.json');
    return this.http.get(environment.base_url + 'error/max-error-tables?error_desc=' + param.error_desc + '&' + param.date)
  }
  
  getTablesList(param) {
    //return this.http.get('/assets/data/tableList.json');
    return this.http.get(environment.base_url + 'filter/tables?schema=' + param)
  }

  getReportingTableData1(param) {
    //return this.http.get('/assets/data/reporting-table1.json');
    return this.http.get(environment.base_url + 'error/current-vs-prior?' + param);
  }

  getReportingTableData2(param) {
    if(param.length == 0) {
      return this.http.get(environment.base_url + 'error/error-details');
    } else {
      return this.http.get(environment.base_url + 'error/error-details?rolling_period=' + param);
    }
  }

  validateConnection(param) {
    console.log(param, Object.keys(param).length);
    if(Object.keys(param).length > 0) {
      return this.http.get(environment.base_url + 'validate/connect?warehouse=' + param.warehouse + '&user=' + param.user + '&password=' + param.password + '&data_warehouse=' + param.data_warehouse);
    } else {
      return this.http.get(environment.base_url + 'validate/connect');
    }
  }
}
