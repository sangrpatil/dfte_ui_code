import { Component, OnInit, ViewChild } from '@angular/core';
import {Router} from '@angular/router';
import {RestApiService} from '../rest-api.service';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';

export interface tableData {
  Batch_date: string, 
  Validation_Date: string,
  Table_Name: string, 
  Error_Description: string, 
  Error_Count: string, 
  Error_Severity: string
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})


export class LoginComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  showSummaryTable = true;
  summaryText = "Validation is in progess..";
  details = {
    warehouseType : "",
    validationType: "",
    username: "",
    pswd: "",
    schema: "",
    table_to_validate: "",
    warehouse: "",
    server: ""
  }
  connectionSet = "fasle";
  schemaList = ["STAGE", "HYBRIS_BI" , "SOURCE_RAW"];
  validationType = ["Mandatory field is missing","Numeric Data Type Check","Duplicates in Primary Key/Composite Key","Date Format Check", "Negative Value Check", "SCD Check", "Special Characters", "True Duplicates"]
  warehouseType = [
    {
      "title" : "Snowflake",
      "value" : "Snowflake"
    },
    {
      "title" : "AWS Redshift",
      "value" : "redshift"
    },
    {
      "title" : "MS Azure",
      "value" : "azure"
    },
    {
      "title" : "GCP Big Query",
      "value" : "gcp"
    }
  ]
  isValidated;
  table_data: any[]; 
  tableList;
  columns: string[] = [
    "Batch_date",     
    "Validation_Date",
    "Table_Name",
    "Error_Description",  
    "Error_Count", 
    "Error_Severity"
  ]
  datasource: any;

  constructor(private router: Router, private restApi: RestApiService) { }

  ngOnInit() {
  }

  schemaChanged(value) {
    this.restApi.getTablesList(value).subscribe(res=> {
      this.tableList = res["data"];
    })
  }

  testConnection() {
    let param = {
      "data_warehouse": this.details.warehouseType,
      "user": this.details.username,
      "password": this.details.pswd,
      "warehouse": this.details.warehouse
    }
    this.restApi.validateConnection(param).subscribe(res=> {
      if(res["status"]== "Success") {
        this.connectionSet = "success";
      } else {
        this.connectionSet = "error";
      }
    })
  }

  isTestDisabled() {
    if(this.details.warehouseType == "Snowflake") {
      return this.details.pswd.length>0 && this.details.username.length>0 && this.details.warehouse.length>0;
    } else {
      return this.details.pswd.length>0 && this.details.username.length>0 && this.details.server.length>0;
    }
  }

  explore() {
    this.isValidated = "true";
    this.showSummaryTable = false;
    this.summaryText = "Validation is in progess..";
    let data = {
      "schema" : this.details.schema,
      "table" : this.details.table_to_validate,
      "validation_rule" : this.details.validationType 
    }
    
    this.restApi.getExecutionSummary(data).subscribe(res => {
      if(res["data"].length == 0) {
        this.showSummaryTable = false;
        this.summaryText = "No validation error for selected criteria";
      } else {
        this.showSummaryTable = true;
        this.table_data = res["data"];
        this.datasource = new MatTableDataSource<tableData>(this.table_data);
        setTimeout(() => (this.datasource.paginator = this.paginator));
      }
    })
  }

}
